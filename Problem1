### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ a11f2233-97b3-4e7f-bad2-cf1ca572adf1
using Markdown

# ╔═╡ ad9ea8f5-5280-45c1-a158-840f45a72f3a
using InteractiveUtils

# ╔═╡ d12453a8-7f04-4a81-9e6b-6cbe608c0f9c
using PlutoUI

# ╔═╡ 8eae5330-5aa1-44df-b2ae-b5d0d38e1502
using DataStructures

# ╔═╡ 9441fc6d-92f5-4584-baa4-556dcf688dbd
md"# Second Assignment Problem 1"

# ╔═╡ 5668c1a8-220c-4a16-bfeb-086319eb95ce
md"## State definitions "

# ╔═╡ 660a3ad1-83ed-4a8a-ba1e-ac6c928729a9
struct State
    name::String
    position::Int64
   parcel::Vector{Bool}
end

# ╔═╡ 4643f83a-a48f-4bbc-be01-8a54c121966f
md"## Action Definition "

# ╔═╡ cd925ac9-84b9-4cdc-b648-0a2199b99621
struct Action
    name::String
    cost::Int64
end

# ╔═╡ 521204d1-260e-44bf-ab10-805717c39608
md"## Assigning actions to the building "

# ╔═╡ 5af43442-153c-49e2-a3df-6e0aeac872bb
Action1 = Action("move east", 2)

# ╔═╡ 56011f18-828d-49d0-9578-195f3caf2b68
Action2 = Action("move west", 2)

# ╔═╡ 7343e8f2-e9e8-4cc4-8770-ff7db2b04248
Action3 = Action("move up", 1)

# ╔═╡ b25c954b-1657-4879-84ed-d454edea8fb8
Action4 = Action("move down", 1)

# ╔═╡ 331574d9-4a48-4e09-8d5a-fee7d4cbf44d
Action5 = Action("collect", 5)

# ╔═╡ 7d5c21e1-ea84-43d4-81b8-4ed0b098c7a3
md"## Assigning the states to the building  "

# ╔═╡ a057c3fb-6e58-4fea-9b81-f575e7f1bb8a
State1 = State("State 1", 1, [true, false, true])

# ╔═╡ a0a6bd68-a50d-4d6c-b091-db927dde29cd
State2 = State("State 2", 2, [true, true, false])

# ╔═╡ 78382f30-9b24-459c-8f97-4325459c150c
State3 = State("State 3", 3, [false, true, true])

# ╔═╡ 377fd9cc-e0e8-4957-b50e-43411edbe43f
State4 = State("State 4", 4, [false, true, true])

# ╔═╡ 8085fc2c-7931-46c9-ae9d-ded1fe2d2d36
State5 = State("State 5", 5, [true, false, true])

# ╔═╡ 5eac2127-2048-4c6b-ab6b-e60c2c5f45fe
State6 = State("State 6", 1, [false, true, true])

# ╔═╡ 865423c6-70d4-4aef-bd93-bb3f79652fc9
State7 = State("State 7", 2, [false, false, true])

# ╔═╡ 4dd9a6ae-4bec-424d-8262-04533ea6e49f
State8 = State("State 8", 3, [false, false,true])

# ╔═╡ b723e536-92d6-43aa-8487-a8ba7ba0d833
State9 = State("State 9", 4, [false, false, true])

# ╔═╡ 862e3c31-b154-441c-b217-c08337f1a94f
State10 = State("State 10", 5, [false, false, true])

# ╔═╡ 57ea78a5-a025-4903-8d00-39f650edb395
md"### Creating transition model"

# ╔═╡ 2a814bab-1317-4bc1-a821-98c39aa56807
TransitionModel = Dict()

# ╔═╡ 9abfb6f1-4577-40ff-a294-5571caaf2671
md"### Add a mapping/push! to the transition Model"

# ╔═╡ e1506d98-1ed1-4c0a-83d0-c9fece9afeff
push!(TransitionModel, State1 => [(Action2, State2), (Action5, State6), (Action3, State1)])

# ╔═╡ 7f21cea4-4b6d-4752-9a9a-850379d8985e
push!(TransitionModel, State1 => [(Action2, State3), (Action5, State7), (Action1, State2)])

# ╔═╡ a7d2e820-13d7-4496-8686-a913e8a3cc7d
push!(TransitionModel, State1 => [(Action4, State4), (Action5, State8), (Action1, State3)])

# ╔═╡ 4a331a45-5ca6-49c2-9d84-30440b53e5ce
push!(TransitionModel, State1 => [(Action2, State5), (Action5, State9), (Action3, State4)])

# ╔═╡ a57cc21f-a712-4585-a62f-5f82f5ecd89d
push!(TransitionModel, State9 => [(Action2, State5), (Action5, State10), (Action4, State10)])

# ╔═╡ f8efd6d2-97e8-4ad0-98a8-987eeca5e116
md"##### Calling transition model"

# ╔═╡ 28a315a3-ce66-4823-b67d-9d64a7b5032e
TransitionModel

# ╔═╡ 1c1058d3-04a4-45eb-b28c-63bf191dcece
md"### The Initial State"

# ╔═╡ c17fa1f1-46c4-415a-8964-57624e4501d1
function coroutine(c::Channel)
    State = rand(100)
    for s in State
    	push!(c, s)
    end
    return
end

# ╔═╡ a47256ae-7838-4ac1-bcb9-081a5e6ac1fd
c = Channel(1)

# ╔═╡ 2283ff13-3f7b-485a-af13-a2cdcee5fd06
t = @async coroutine(c)

# ╔═╡ 8c7e10b3-5ca9-4822-8c2b-b3bb82f03c1f
take!(c)

# ╔═╡ 2801b454-6923-43d3-bbae-f778c28cde02
md"### The Goal State"

# ╔═╡ 4599a64c-9c67-494b-af36-05305be41f23
function goalState()
  next = iterate(iter)
	while next !== nothing
    (i, State) = next
    # body
    next = iterate(iter, State)
	end
end

# ╔═╡ 19169fb2-f973-4d00-93a4-2d87b6edc305
md"## Astar Search algorithm"

# ╔═╡ f534a886-a582-4c6e-8dec-67a0d5f73035
function heuristic(currentState, goalState)
    return distance(currentState[1] - goalState[1]) + distance(cell[2] - goalState[2])
end

# ╔═╡ 07d91764-9768-48f0-b525-cf1a82477bdf
function AstarSearch(TransitionModel,initialState, goalState)
	
    result = []
	frontier = Queue{State}()
	explored = []
	parents = Dict{State, State}()
	first_state = true
    enqueue!(frontier, initialState)
	parent = initialState
	
    while true
		if isempty(frontier)
			return []
		else
			currentState = dequeue!(frontier)
			push!(explored, currentState)
			candidates = TransitionModel[currentState]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(parents, single_candidate[2] => currentState)
					if (single_candiadte[2] in goal_state)
						return get_result(TransitionModel, parents,initialState, single_candidate[2])
					else
						enqueue!(frontier, single_candidate[2])
					end
				end
			end
		end
	end
end

# ╔═╡ ae36336b-1908-43e3-a169-d3d0c74c34a0
function result(TransitionModel, ancestors, initialState, goalState)
	result = []
	explorer = goalState
	while !(explorer == initialState)
		current_state_ancestor = ancestors[explorer]
		related_transitions = TransitionModel[current_state_ancestor]
		for single_trans in related_transitions
			if single_trans[2] == explorer
				push!(result, single_trans[1])
				break
			else
				continue
			end
		end
		explorer = current_state_ancestor
	end
	return result
end

# ╔═╡ 643afd18-f419-4010-a5f4-9914c3d1286b
function findsearch(initialState, transition_dict, is_goal,all_candidates,
add_candidate, remove_candidate)
	explored = []
	ancestors = Dict{State, State}()
	the_candidates = add_candidate(all_candidates, initialState, 0)
	parent = initialState
	while true
		if isempty(the_candidates)
			return []
		else
			(t1, t2) = remove_candidate(the_candidates)
			current_state = t1
			the_candidates = t2
#proceed with handling the current state
			push!(explored, current_state)
			candidates = transition_dict[current_state]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(ancestors, single_candidate[2] => current_state)
					if (is_goal(single_candidate[2]))
						return result(transition_dict, ancestors,
initialState, single_candidate[2])
					else
						the_candidates = add_candidate(the_candidates,
single_candidate[2], single_candidate[1].cost)
						end
					end
				end
			end
		end
end

# ╔═╡ 13a064c1-9321-4660-a5a4-10fb260536a2
function goal_test(currentState::State)
    return ! (currentState.parcel[1] || currentState.parcel[2])
end

# ╔═╡ 3e292e53-08b8-4be0-91af-24877679ddf2
md"# Implementation of Company X API"

# ╔═╡ 9db5f5a7-a413-4bbe-a49c-19fd4ebc2ce9
function add_to_queue(queue::Queue{State}, state::State, cost::Int64)
    enqueue!(queue, state)
    return queue
end

# ╔═╡ 2c64271a-17bc-4fb3-9ab8-41ff7c14a2fa
function add_to_stack(stack::Stack{State}, state::State, cost::Int64)
    push!(stack, state)
    return stack
end

# ╔═╡ e77419b3-05c4-4531-971b-512f200d1fe0
function remove_from_queue(queue::Queue{State})
    removed = dequeue!(queue)
    return (removed, queue)
end

# ╔═╡ 81c0a1df-a940-4632-8619-6220a6f4a8bc
function remove_from_stack(stack::Stack{State})
    removed = pop!(stack)
    return (removed, stack)
end

# ╔═╡ 6a07d8e2-0751-42c2-a1f3-309fb4a5c9c0
findsearch(State1, TransitionModel, goal_test, Queue{State}(), add_to_queue, remove_from_queue)

# ╔═╡ bc6d86c0-9f16-4f9b-aeed-b5ee9e3be8a1
@bind storeys TextField()

# ╔═╡ a1eea973-9260-492f-8481-4fafdef7659d
with_terminal() do
	println("Number of storeys are:",storeys)
end

# ╔═╡ 3e5e416f-86d6-42ba-86ed-b02161c28d01
@bind offices TextField()

# ╔═╡ 275c06e1-f2bc-4100-96c8-182368ef0e0e
with_terminal() do
	println("Number of occupied offices per floor are:",offices)
end

# ╔═╡ 241b0a34-27e8-40e4-bdc8-7cc6f335369f
@bind parcels TextField()

# ╔═╡ 91b060e0-6036-4d7d-8d43-99c1daadb17c
with_terminal() do
	println("Number of parcels in each office are:", parcels)
end

# ╔═╡ d46d74fe-32e0-4754-ae57-2ca62ddc89e6
@bind location Radio(["State1", "State2", "State3"])

# ╔═╡ e416baac-274a-4d38-95d7-f69bfb7d2775
with_terminal() do
	println("Location of the agent at the beginning is:",location)
end

# ╔═╡ c546aef8-bf9a-4902-a6c1-75a4f91683ae
md"### Implementation of artificial intelligence agent"

# ╔═╡ b0cd004d-7806-44b3-b115-e098deee77c9
struct agent
  
end

# ╔═╡ 07600e77-b579-4ee2-ac7b-4867e322b24e
function parcel()
    empty = true
end

# ╔═╡ 398f0bfd-d9b0-4e7b-866d-d7c4e4f67d0f
function collect(X)
    "'''collect parcel x if x is empty'''"
    if not empty == true 
       throw(NotEmptyException)
    empty = x
    end
end

# ╔═╡ b40dcb57-dff4-4387-acad-bdac45d7af4a
function drop()
    "'''drops the parcel in hand'''"
    if not empty 
     throw(NoParcelInHandExceptionthrow)
     empty = true
	end
end

# ╔═╡ Cell order:
# ╠═9441fc6d-92f5-4584-baa4-556dcf688dbd
# ╠═a11f2233-97b3-4e7f-bad2-cf1ca572adf1
# ╠═ad9ea8f5-5280-45c1-a158-840f45a72f3a
# ╠═d12453a8-7f04-4a81-9e6b-6cbe608c0f9c
# ╠═8eae5330-5aa1-44df-b2ae-b5d0d38e1502
# ╠═5668c1a8-220c-4a16-bfeb-086319eb95ce
# ╠═660a3ad1-83ed-4a8a-ba1e-ac6c928729a9
# ╠═4643f83a-a48f-4bbc-be01-8a54c121966f
# ╠═cd925ac9-84b9-4cdc-b648-0a2199b99621
# ╠═521204d1-260e-44bf-ab10-805717c39608
# ╠═5af43442-153c-49e2-a3df-6e0aeac872bb
# ╠═56011f18-828d-49d0-9578-195f3caf2b68
# ╠═7343e8f2-e9e8-4cc4-8770-ff7db2b04248
# ╠═b25c954b-1657-4879-84ed-d454edea8fb8
# ╠═331574d9-4a48-4e09-8d5a-fee7d4cbf44d
# ╠═7d5c21e1-ea84-43d4-81b8-4ed0b098c7a3
# ╠═a057c3fb-6e58-4fea-9b81-f575e7f1bb8a
# ╠═a0a6bd68-a50d-4d6c-b091-db927dde29cd
# ╠═78382f30-9b24-459c-8f97-4325459c150c
# ╠═377fd9cc-e0e8-4957-b50e-43411edbe43f
# ╠═8085fc2c-7931-46c9-ae9d-ded1fe2d2d36
# ╠═5eac2127-2048-4c6b-ab6b-e60c2c5f45fe
# ╠═865423c6-70d4-4aef-bd93-bb3f79652fc9
# ╠═4dd9a6ae-4bec-424d-8262-04533ea6e49f
# ╠═b723e536-92d6-43aa-8487-a8ba7ba0d833
# ╠═862e3c31-b154-441c-b217-c08337f1a94f
# ╠═57ea78a5-a025-4903-8d00-39f650edb395
# ╠═2a814bab-1317-4bc1-a821-98c39aa56807
# ╠═9abfb6f1-4577-40ff-a294-5571caaf2671
# ╠═e1506d98-1ed1-4c0a-83d0-c9fece9afeff
# ╠═7f21cea4-4b6d-4752-9a9a-850379d8985e
# ╠═a7d2e820-13d7-4496-8686-a913e8a3cc7d
# ╠═4a331a45-5ca6-49c2-9d84-30440b53e5ce
# ╠═a57cc21f-a712-4585-a62f-5f82f5ecd89d
# ╠═f8efd6d2-97e8-4ad0-98a8-987eeca5e116
# ╠═28a315a3-ce66-4823-b67d-9d64a7b5032e
# ╠═1c1058d3-04a4-45eb-b28c-63bf191dcece
# ╠═c17fa1f1-46c4-415a-8964-57624e4501d1
# ╠═a47256ae-7838-4ac1-bcb9-081a5e6ac1fd
# ╠═2283ff13-3f7b-485a-af13-a2cdcee5fd06
# ╠═8c7e10b3-5ca9-4822-8c2b-b3bb82f03c1f
# ╠═2801b454-6923-43d3-bbae-f778c28cde02
# ╠═4599a64c-9c67-494b-af36-05305be41f23
# ╠═19169fb2-f973-4d00-93a4-2d87b6edc305
# ╠═f534a886-a582-4c6e-8dec-67a0d5f73035
# ╠═07d91764-9768-48f0-b525-cf1a82477bdf
# ╠═ae36336b-1908-43e3-a169-d3d0c74c34a0
# ╠═643afd18-f419-4010-a5f4-9914c3d1286b
# ╠═13a064c1-9321-4660-a5a4-10fb260536a2
# ╠═3e292e53-08b8-4be0-91af-24877679ddf2
# ╠═9db5f5a7-a413-4bbe-a49c-19fd4ebc2ce9
# ╠═2c64271a-17bc-4fb3-9ab8-41ff7c14a2fa
# ╠═e77419b3-05c4-4531-971b-512f200d1fe0
# ╠═81c0a1df-a940-4632-8619-6220a6f4a8bc
# ╠═6a07d8e2-0751-42c2-a1f3-309fb4a5c9c0
# ╠═bc6d86c0-9f16-4f9b-aeed-b5ee9e3be8a1
# ╠═a1eea973-9260-492f-8481-4fafdef7659d
# ╠═3e5e416f-86d6-42ba-86ed-b02161c28d01
# ╠═275c06e1-f2bc-4100-96c8-182368ef0e0e
# ╠═241b0a34-27e8-40e4-bdc8-7cc6f335369f
# ╠═91b060e0-6036-4d7d-8d43-99c1daadb17c
# ╠═d46d74fe-32e0-4754-ae57-2ca62ddc89e6
# ╠═e416baac-274a-4d38-95d7-f69bfb7d2775
# ╠═c546aef8-bf9a-4902-a6c1-75a4f91683ae
# ╠═b0cd004d-7806-44b3-b115-e098deee77c9
# ╠═07600e77-b579-4ee2-ac7b-4867e322b24e
# ╠═398f0bfd-d9b0-4e7b-866d-d7c4e4f67d0f
# ╠═b40dcb57-dff4-4387-acad-bdac45d7af4a
